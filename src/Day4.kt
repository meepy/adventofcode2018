

/* sample data
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
 */


class Day4(rawInput: List<String>) : Day(rawInput) {

    val guards = mutableMapOf<Int, IntArray>()

    val parseID = """Guard #(\d+) """.toRegex()
    val parseSleeps = """00:(\d\d)] falls""".toRegex()
    val parseWakes = """00:(\d\d)] wakes""".toRegex()

    override fun part1(): Any? {
        var currentID = -1
        var sleepTime = -1

        for (line in rawInput.sorted()) {
            parseID.find(line)?.let {
                currentID = it.groupValues[1].toInt()
                if (!guards.containsKey(currentID))
                    guards[currentID] = IntArray(60)

            } ?: parseSleeps.find(line)?.let {

                sleepTime = it.groupValues[1].toInt()

            } ?: parseWakes.find(line)?.let {

                val wakeTime = it.groupValues[1].toInt()
                val guardArray = guards[currentID]!!
                for (i in sleepTime until wakeTime) {
                    guardArray[i] += 1
                }
            } ?: throw Exception("bad input: $line")
        }

//        for ((id, times) in guards) {
//            val timeString = times.joinToString(",")
//            println("$id: $timeString")
//        }

        val (maxID, maxTimes) = guards.maxBy { (id, times) ->
            times.sum()
        }!!

        val maxIndex = maxTimes.indexOf(maxTimes.max()!!)
        return "part1: $maxID * $maxIndex = ${maxID * maxIndex}"
    }

    override fun part2(): Any? {
        val (bestGuard, bestTime) = guards.map { (id, times) ->
            id to times.max()!!
        }.maxBy { (id, maxTime) ->
            maxTime
        }!!

        val bestIndex = guards[bestGuard]!!.indexOf(bestTime)
        return "part2: $bestGuard * $bestIndex = ${bestGuard * bestIndex}"
    }
}
