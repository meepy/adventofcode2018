import kotlin.math.abs

class Day6(rawInput: List<String>) : Day(rawInput) {

    data class Point(var x: Int, var y: Int) {
        fun mDistance(x2: Int, y2: Int): Int {
            return abs(x2 - x) + abs(y2 - y)
        }
    }

    data class BestTarget(var dist: Int, var index: Int)

    val targets: List<Point> = rawInput.map {
           val (x, y) = """(\d+), (\d+)""".toRegex().matchEntire(it)?.destructured  ?: throw Exception("bad input")
           Point(x.toInt(), y.toInt())
       }

    val topLeft = Point(targets.minBy { it.x }!!.x, targets.minBy { it.y }!!.y)
    val bottomRight = Point(targets.maxBy { it.x }!!.x, targets.maxBy { it.y }!!.y)

    val bounds = Point(bottomRight.x - topLeft.x + 1, bottomRight.y - topLeft.y + 1)


    val nearestTargets = List(bounds.y) {
        List(bounds.x) { Day6.BestTarget(Integer.MAX_VALUE, -1) }  // min distance and target
    }

    val distanceSums = List(bounds.y) {
        IntArray(bounds.x)
    }

    val targetAreas = IntArray(targets.size)

    init {
        targets.forEach { pt ->  // normalize
            pt.x -= topLeft.x
            pt.y -= topLeft.y
        }
    }

    private fun eliminateEdges() {
        nearestTargets.first().forEach {
            if (it.index >= 0)
                targetAreas[it.index] = -1
        }
        nearestTargets.last().forEach {
            if (it.index >= 0)
                targetAreas[it.index] = -1
        }
        nearestTargets.forEach {
            val target = it.first().index
            if (target >= 0)
                targetAreas[target] = -1
        }
        nearestTargets.forEach {
            val target = it.last().index
            if (target >= 0)
                targetAreas[target] = -1
        }
    }

    override fun part1(): Any? {
        nearestTargets.forEachIndexed { y, line ->
            line.forEachIndexed { x, current ->
                targets.forEachIndexed { index, pt ->
                    val dist = pt.mDistance(x, y)
                    if (current.dist > dist) {
                        current.dist = dist
                        current.index = index
                    } else if (current.dist == dist)
                        current.index = -1
                }
            }
        }

        nearestTargets.forEach {
            it.forEach { (_, index) ->
                if (index >= 0)
                    targetAreas[index] += 1
            }
        }

        eliminateEdges()

        return targetAreas.max()
    }

    override fun part2(): Any? {
        // check corner values >= 10k to make sure we cover the whole area
        println("topleft: ${targets.sumBy {it.x + it.y}}")
        println("bottomright: ${targets.sumBy {bounds.x - it.x + bounds.y - it.y}}")
        println("bottomleft: ${targets.sumBy {it.x + bounds.y - it.y}}")
        println("topRight: ${targets.sumBy {bounds.x - it.x + it.y}}")

        for (y in 0 until bounds.y) {
            for (x in 0 until bounds.x) {
                distanceSums[y][x] = targets.sumBy { pt ->
                     pt.mDistance(x, y)
                }
            }
        }

        val numPoints = distanceSums.sumBy { line ->
            line.count { it < 10000 }
        }

        return numPoints
    }
}