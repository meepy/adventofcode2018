class Day13(rawInput: List<String>) : Day(rawInput) {

    enum class TrackType(val char: Char, val dX: Int = 0, val dY: Int = 0) {
        UPDOWN('|'), LEFTRIGHT('-'),
        TURN1('/'), TURN2('\\'),
        INTERSECTION('+'), EMPTY(' '),
        CARLEFT('<', -1, 0), CARRIGHT('>', 1, 0), CARDOWN('v', 0, 1), CARUP('^', 0, -1);

        companion object {
            val fromChar = values().map{ it.char to it }.toMap()
        }
    }

    data class Cart(var x: Int, var y: Int, var dX: Int, var dY: Int) {
        var nextTurn = -1 // -1: left turn, 0: straight, 1: right turn
        var isCrashed = false
    }

    val maxX = rawInput.maxBy { it.length }!!.length
    val maxY = rawInput.size
    val tracks = Array(maxY) { Array(maxX) { TrackType.EMPTY } }
    val carts = mutableListOf<Cart>()
    val cartPositions = Array(maxY) { Array<Cart?>(maxX) {null} }

    init {
        for ((y, line) in rawInput.withIndex()) {
            for ((x, char) in line.withIndex()) {
                val type = TrackType.fromChar[char]!!
                when (type) {
                    TrackType.CARLEFT, TrackType.CARRIGHT, TrackType.CARDOWN, TrackType.CARUP -> {
                        val newCart = Cart(x, y, type.dX, type.dY)
                        carts.add(newCart)
                        cartPositions[y][x] = newCart
                    }
                    else -> Unit
                }

                tracks[y][x] = when (type) {
                    TrackType.CARLEFT, TrackType.CARRIGHT -> TrackType.LEFTRIGHT
                    TrackType.CARDOWN, TrackType.CARUP -> TrackType.UPDOWN
                    else -> type
                }
            }
        }
        /*
        for (row in tracks) {
            println(row.map { it.char }.joinToString(separator = ""))
        }

        for (cart in carts)
            println(cart)
        */
    }

    private fun step(carts: Array<Cart>, cartPositions: Array<Array<Cart?>>,
                     stopAtFirst: Boolean = false): String? {

        carts.sortBy { it.x * maxY + it.y }
        for (cart in carts) {
            if (cart.isCrashed)
                continue

            cartPositions[cart.y][cart.x] = null
            cart.x += cart.dX
            cart.y += cart.dY
            if (cartPositions[cart.y][cart.x] != null) {
                println("found collision at: ${cart.x},${cart.y}")
                cart.isCrashed = true
                cartPositions[cart.y][cart.x]!!.isCrashed = true
                cartPositions[cart.y][cart.x] = null
                if (stopAtFirst)
                    return "found collision: ${cart.x},${cart.y}"
            } else {
                cartPositions[cart.y][cart.x] = cart
            }

            when (tracks[cart.y][cart.x]) {
                TrackType.TURN1 -> { // '/'
                    val x = -cart.dX
                    cart.dX = -cart.dY
                    cart.dY = x
                }
                TrackType.TURN2 -> { // '\'
                    val x = cart.dX
                    cart.dX = cart.dY
                    cart.dY = x
                }
                TrackType.INTERSECTION -> {
                    if (cart.nextTurn != 0) {
                        val x = cart.nextTurn * cart.dX
                        cart.dX = -cart.nextTurn * cart.dY
                        cart.dY = x
                    }
                    cart.nextTurn = (cart.nextTurn + 2) % 3 - 1
                }
                TrackType.LEFTRIGHT, TrackType.UPDOWN -> Unit
                else -> throw Exception("reached invalid track type")
            }
        }

        return null
    }

    override fun part1(): Any? {
        val cartsCopy = with(carts) { Array(size) { get(it).copy() } }
        val positionsCopy = with(cartPositions) { Array(size) {get(it).clone()} }

        var answer: String? = null
        while (answer == null)  {
            answer = step(cartsCopy, positionsCopy, stopAtFirst = true)
        }

        return answer
    }

    override fun part2(): Any? {

        while (carts.filter { !it.isCrashed }.size > 1) {
            step(carts.toTypedArray(), cartPositions)
        }
        return carts.first { !it.isCrashed }
    }
}