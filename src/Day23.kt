
import java.awt.Color
import java.awt.Graphics
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_INT_RGB
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JFrame.EXIT_ON_CLOSE
import javax.swing.JLabel
import javax.swing.JPanel
import kotlin.math.abs
import kotlin.math.max

typealias Point3D = Triple<Long, Long, Long>
typealias Point2D = Pair<Long, Long>

const val IMG_WIDTH = 500
const val IMG_HEIGHT = 500

fun Point3D.sum() = first + second + third

fun mDist(v1: Point3D, v2: Point3D): Long {
    return abs(v1.first - v2.first) + abs(v1.second - v2.second) +
            abs(v1.third - v2.third)
}

class Day23(rawInput: List<String>) : Day(rawInput) {

//    pos=<-27458727,37082345,47925315>, r=95853959
//    pos=<64849283,81764276,20735258>, r=87673726

    val positions: MutableList<Pair<Point3D,  Long>> = mutableListOf()

    init {
        val parser = """pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)""".toRegex()
        for (line in rawInput) {
            val (x, y, z, r) = parser.matchEntire(line)?.destructured ?:
                    throw Exception("bad input: $line")
            positions.add(Triple(x.toLong(), y.toLong(), z.toLong()) to r.toLong())
        }
    }

    class MIntersection {
        val bots = mutableListOf<Pair<Point3D, Long>>()

        companion object {
            fun create(position: Pair<Point3D, Long>) = MIntersection().apply {
                bots.add(position)
            }
        }

        fun doesIntersect(position: Pair<Point3D, Long>): Boolean {
            return !bots.contains(position) && bots.all {
                mDist(it.first, position.first) <= it.second + position.second
            }
        }

        fun addAll(other: List<Pair<Point3D, Long>>): MIntersection {
            bots.addAll(other)
            return this
        }

        fun intersect(position: Pair<Point3D, Long>): MIntersection {
            return create(position).addAll(bots)
        }
    }

    override fun part1(): Any? {
        val bestBot = positions.maxBy { it.second }!!
        return positions.count { mDist(it.first, bestBot.first) <= bestBot.second }
    }

    override fun part2(): Any? {
        /*
        var bestAreas = mutableListOf<MIntersection>()

        for ((index, position) in modules.withIndex()) {
            val newAreas = mutableListOf<MIntersection>()
            for (area in bestAreas) {
                if (area.doesIntersect(position))
                    newAreas.add(area.intersect(position))
            }
            newAreas.add(MIntersection.create(position))
            println("$index: ${newAreas.size}")
            bestAreas.addAll(newAreas)
        }
        */

//        val area = listOf(modules.maxBy { it.first.first }!!.first.first, modules.minBy { it.first.first }!!.first.first,
//        modules.maxBy { it.first.second }!!.first.second, modules.minBy { it.first.second }!!.first.second,
//        modules.maxBy { it.first.third }!!.first.third, modules.minBy { it.first.third }!!.first.third)

        val outsideOrigin = positions.filter { (origin, radius) ->
            mDist(origin, Point3D(0, 0, 0)) > radius
        }
        val aboveXY = outsideOrigin.filter { (origin, radius) ->
            origin.third > 0
        }
        val belowXY = outsideOrigin.filter { (origin, radius) ->
            origin.third < 0
        }

        val sameSignAbove = aboveXY.filter { (origin, radius) ->
            origin.first > 0 && origin.second > 0 // || origin.first < 0 && origin.second < 0
        }
        val oppositeSignAbove = aboveXY.filter { (origin, radius) ->
            origin.first > 0 && origin.second < 0 || origin.first < 0 && origin.second > 0
        }
        val sameSignBelow = belowXY.filter { (origin, radius) ->
            origin.first > 0 && origin.second > 0 || origin.first < 0 && origin.second < 0
        }
        val oppositeSignBelow = belowXY.filter { (origin, radius) ->
            origin.first > 0 && origin.second < 0 || origin.first < 0 && origin.second > 0
        }

        val zOverlap = sameSignAbove.map { (origin, radius) ->
            positions.count { (origin2, radius2) ->
                mDist(origin, origin2) <= radius + radius2
            }
        }

        println("num: ${positions.count { (origin, radius) ->
            mDist(origin, solution) <= radius
        }}")

        positions.forEach {  (origin, radius) ->
            if (mDist(origin, solution) == radius) {
                println("Point3D(${origin.first}L, ${origin.second}L, ${origin.third}L), ${radius}L)")
                if ((mDist(origin, Point3D(solution.first + 1, solution.second, solution.third)) > radius) &&
                     (mDist(origin, Point3D(solution.first, solution.second + 1, solution.third)) > radius) &&
                         (mDist(origin, Point3D(solution.first, solution.second, solution.third + 1)) > radius)) {
                    println("above on top right")
                }
                if ((mDist(origin, Point3D(solution.first + 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second - 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third + 1)) > radius)) {
                    println("above on bottom right")
                }
                if ((mDist(origin, Point3D(solution.first - 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second + 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third + 1)) > radius)) {
                    println("above on top left")
                }
                if ((mDist(origin, Point3D(solution.first - 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second - 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third + 1)) > radius)) {
                    println("above on bottom left")
                }
                if ((mDist(origin, Point3D(solution.first + 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second + 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third - 1)) > radius)) {
                    println("below on top right")
                }
                if ((mDist(origin, Point3D(solution.first + 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second - 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third - 1)) > radius)) {
                    println("below on bottom right")
                }
                if ((mDist(origin, Point3D(solution.first - 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second + 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third - 1)) > radius)) {
                    println("below on top left")
                }
                if ((mDist(origin, Point3D(solution.first - 1, solution.second, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second - 1, solution.third)) > radius) &&
                    (mDist(origin, Point3D(solution.first, solution.second, solution.third - 1)) > radius)) {
                    println("below on bottom left")
                }

//                    &&
//                   origin.first >= 0 && origin.second >= 0 &&
//                    origin.third >= 0) {
//                println("Point3D(${origin.first}L, ${origin.second}L, ${origin.third}L), ${radius}L)")
//                println("${origin.first <= solution.first},${origin.second <= solution.second},${origin.third <= solution.third}")
            }
        }

        println("dist: ${mDist(solution, Point3D(73676460L, 33821245L, 52735523L))}, radius: 74471686")

        /*
        val allMaxes = sameSignAbove.map { position ->
            findMax(position)
        }
        val best = sameSignAbove[allMaxes.indexOf(allMaxes.max())]
        */

        val best = Pair(
        Point3D(-36820422L, 25273367L, 30557170L), 80308822L)
//        below on top right
            /*
below on bottom right
Point3D(27218509L, 19302718L, 68251100L), 59934470L)
below on top right
Point3D(2890405L, 49922902L, -4498164L), 82474928L)
above on bottom right
Point3D(26402737L, 50092949L, 47439366L), 54516899L)
below on bottom right
Point3D(6978024L, 46460359L, 70863058L), 93732714L)
below on bottom right
Point3D(4649316L, 49361681L, 32740988L), 60840674L)
below on bottom right
Point3D(15745953L, 67535503L, 46213634L), 81390505L)
below on bottom right
Point3D(53359792L, 51879423L, 11826100L), 55208518L)
above on bottom left
Point3D(23391152L, 10203853L, 48862159L), 53471751L)
below on top right
Point3D(-52142356L, 33930847L, 25845905L), 95306429L)
below on bottom right
Point3D(20324862L, 41478172L, 83615874L), 88156505L)
below on bottom right
Point3D(-13707431L, 38985066L, 33318241L), 69398059L)
below on bottom right
Point3D(20507211L, 86696884L, 19127290L), 78006650L)
above on bottom right
Point3D(23263339L, 62410065L, 61632541L), 84166588L)
below on bottom right
Point3D(-36820422L, 25273367L, 30557170L), 80308822L)
below on top right
Point3D(5232442L, 38718591L, 37204134L), 54077604L)
below on bottom right
Point3D(-4231482L, 33231741L, 39303886L), 60154430L)
below on bottom right
Point3D(22438390L, -7128416L, 60092412L), 82987035L)
below on top right
Point3D(14602695L, 45402638L, 48411065L), 62598329L)
below on bottom right
Point3D(33309923L, 30183286L, 77393014L), 57653698L)
below on bottom right
Point3D(3974868L, 30225940L, 42704258L), 52342651L)
below on bottom right
Point3D(26697891L, 72677780L, 50474572L), 79841782L)
below on bottom right
Point3D(18677091L, 41640155L, 58749742L), 65100127L)
below on bottom right
Point3D(99612382L, 19960521L, 749610L), 95514930L)
above on top left
Point3D(24012160L, 52027781L, 70708770L), 82111712L)
below on bottom right
Point3D(17758857L, 1757826L, 59533695L), 78221609L)
below on top right */

        origin = best.first
        radius = best.second
        topLeft = Point2D(origin.first, origin.second + radius)
        bottomRight = Point2D(origin.first + radius, origin.second)
        setupDrawing()
        drawBest()

        return "$best, ${best.first.sum() - best.second}"
    }

    lateinit var origin: Point3D
    var radius: Long = 0
    lateinit var topLeft: Point2D
    lateinit var bottomRight: Point2D

    val solution = Point3D(34574432, 27408638, 23778473)

    lateinit var rePaint: () -> Unit
    lateinit var g: Graphics

    private fun setupDrawing() {
        class MListener : MouseListener {
            var newTopLeft: Point2D? = null

            override fun mouseExited(p0: MouseEvent?) {
            }

            override fun mouseClicked(p0: MouseEvent?) {
            }

            override fun mouseEntered(p0: MouseEvent?) {
            }

            override fun mouseReleased(p0: MouseEvent?) {
                if (p0 == null)
                    return
                if (p0.x >= IMG_WIDTH || p0.y >= IMG_HEIGHT)
                    return

                val x = topLeft.first + abs(bottomRight.first - topLeft.first) * p0.x / IMG_WIDTH
                val y = topLeft.second - abs(bottomRight.second - topLeft.second) * p0.y / IMG_HEIGHT
                bottomRight = Point2D(x, y)
                topLeft = newTopLeft!!
                println("mouse released: $bottomRight")
                drawBest()
            }

            override fun mousePressed(p0: MouseEvent?) {
                if (p0 == null)
                    return
                if (p0.x >= IMG_WIDTH || p0.y >= IMG_HEIGHT)
                    return

                val x = topLeft.first + abs(bottomRight.first - topLeft.first) * p0.x / IMG_WIDTH
                val y = topLeft.second - abs(bottomRight.second - topLeft.second) * p0.y / IMG_HEIGHT
                newTopLeft = Point2D(x, y)
                println("mouse pressed: $newTopLeft")
            }
        }

        val img = BufferedImage(IMG_WIDTH, IMG_HEIGHT, TYPE_INT_RGB)

        JFrame().apply {
            val panel = JPanel().apply {
                val icon = ImageIcon(img)
                val label = JLabel(icon).apply {
                    addMouseListener(MListener())
                    rePaint = ::repaint
                }
                add(label)
                pack()
            }
            add(panel)
            pack()

            defaultCloseOperation = EXIT_ON_CLOSE
            isVisible = true
        }

        g = img.graphics
    }

    private fun findMax(): Int {
        var maxCount = 0
        for (yImg in 0 until IMG_HEIGHT) {
            val y = topLeft.second - abs(bottomRight.second - topLeft.second) * yImg / IMG_HEIGHT
            for (xImg in 0 until IMG_WIDTH) {
                val x = topLeft.first + abs(bottomRight.first - topLeft.first) * xImg / IMG_WIDTH
                val radiusLeft = radius - abs(origin.first - x) - abs(origin.second - y)
                val z = origin.third - radiusLeft

                if (radiusLeft < 0)
                    continue

                val spot = Point3D(x, y, z)
                val numInside = positions.count { (origin, radius) ->
                    mDist(origin, spot) <= radius
                }
                maxCount = max(maxCount, numInside)
            }
        }
        return maxCount
    }

    private fun dist2(p1: Point3D, p2: Point3D): Long {
        return (p1.first - p2.first) * (p1.first - p2.first) +
         (p1.second - p2.second) * (p1.second - p2.second) +
         (p1.third - p2.third) * (p1.third - p2.third)
    }

    val gradient = (0 .. 1000).map { i ->
        if (i and 1 == 0) {
            Color.getHSBColor(i / 100f, i / 1000f, .6f)
        } else {
            Color.getHSBColor(i / 100f, i / 1000f, .7f)
        }
    }
    val gradient2 = (0 .. 1000).map { i ->
        Color.getHSBColor(i / 1000f, .4f, .4f)
    }

    private fun drawBest() {

        var maxIntersecting = 0
        var closestX = 0
        var closestY = 0
        var closestDist = Long.MAX_VALUE

        var count = 0
        for (yImg in 0 until IMG_HEIGHT) {
            val y = topLeft.second - abs(bottomRight.second - topLeft.second) * yImg / IMG_HEIGHT
            for (xImg in 0 until IMG_WIDTH) {
                val x = topLeft.first + abs(bottomRight.first - topLeft.first) * xImg / IMG_WIDTH
                val radiusLeft = radius - abs(origin.first - x) - abs(origin.second - y)
                val z = origin.third - radiusLeft

                val spot = Point3D(x, y, z)

                val numInside = positions.count { (origin, radius) ->
                    mDist(origin, spot) <= radius
                }
                maxIntersecting = max(numInside, maxIntersecting)

                if (dist2(spot, solution) <= closestDist) {
                    closestDist = dist2(spot, solution)
                    closestX = xImg
                    closestY = yImg
                }

                g.color = when {
                    (radiusLeft < 0 || x < origin.first || y < origin.second || z > origin.third) ->
                        Color.BLACK
                    x < 0 || y < 0 || z < 0 ->
                        gradient[numInside]
                    else ->
                        gradient[numInside]
                }

                g.fillRect(xImg, yImg, 1, 1)
                count++
                if (count % 100 == 0)
                    rePaint()
            }
        }

        g.color = Color.BLUE
        g.drawRect(closestX - 2, closestY - 2, 5, 5)
        rePaint()

        println("max: $maxIntersecting")
    }
}