abstract class Day(val rawInput: List<String>) {
    abstract fun part1(): Any?
    abstract fun part2(): Any?
}