class Day5(rawInput: List<String>) : Day(rawInput) {

    // NOTE: O(n^2)... using a stack is much better than this
    private fun solvePart1(input: String): Int {
        var currPolymer = input
        val nextPolymer = StringBuilder()

        while (currPolymer.length >= 2) {
            var i = 0
            while (i < currPolymer.length - 1) {
                if (currPolymer[i] != currPolymer[i + 1] &&
                    currPolymer[i].toLowerCase() ==
                    currPolymer[i + 1].toLowerCase()
                ) {
                    i += 2
                } else {
                    nextPolymer.append(currPolymer[i])
                    i++
                }
            }
            if (i == currPolymer.length - 1)
                nextPolymer.append(currPolymer[i])


            if (nextPolymer.length != currPolymer.length) {
                currPolymer = nextPolymer.toString()
                nextPolymer.clear()
            } else
                break
        }
        return currPolymer.length
    }

    override fun part1(): Any? {
        return solvePart1(rawInput.first())
    }

    override fun part2(): Any? {
        val lengths = mutableListOf<Int>()
        for (letter in 'a'..'z') {
            val filtered = rawInput.first().filter { it.toLowerCase() != letter }
            lengths.add(solvePart1(filtered))
        }
        return lengths.min()
    }
}