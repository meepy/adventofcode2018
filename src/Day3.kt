
import kotlin.math.max
import kotlin.math.min

class Day3(rawInput: List<String>) : Day(rawInput) {

    class Swatch(val x1: Int, val y1: Int, val x2: Int, val y2: Int, var timesCovered: Int) {

        val children = mutableListOf<Swatch>()

        init {
            if (x1 >= x2 || y1 >= y2)
                throw Exception("invalid swatch: $x1, $y1, $x2, $y2")
        }

        override fun toString(): String {
            return if (children.size == 0) {
                "$x1, $y1, $x2, $y2: $timesCovered\n"
            } else {
                val strBuilder = StringBuilder()
                for (child in children) {
                    strBuilder.append(child.childString(1) + '\n')
                }
                strBuilder.toString()
            }
        }

        private fun childString(indent: Int): String {
            var indention = ""
            for (i in 0 until indent)
                indention += "-"

            return if (children.size == 0) {
                "$indention$x1, $y1, $x2, $y2: $timesCovered\n"
            } else {
                val strBuilder = StringBuilder()
                for (child in children) {
                    strBuilder.append(child.childString(indent + 1) + '\n')
                }
                strBuilder.toString()
            }
        }

        private fun increaseTimesCovered() {
            timesCovered++
            for (child in children)
                child.increaseTimesCovered()
        }

        fun splitBy(sw: Swatch) {
            if (timesCovered > 1) // early bailout ignoring overlays greater than 2
                return

            if (sw.x1 >= x2 || sw.y1 >= y2 ||
                    sw.x2 <= x1 || sw.y2 <= y1) // all outside
                return

            if (sw.x1 <= x1 && sw.y1 <= y1 &&
                sw.x2 >= x2 && sw.y2 >= y2) { // all inside
                increaseTimesCovered()
                return
            }

            if (children.size != 0) {
                for (child in children)
                    child.splitBy(sw)
                return
            }

            if (sw.x1 > x1) {
                val left = Swatch(x1, y1, sw.x1, y2, timesCovered)
                children.add(left)
            }

            if (sw.y1 > y1) {
                val topMiddle = Swatch(max(sw.x1, x1), y1, min(sw.x2, x2), sw.y1, timesCovered)
                children.add(topMiddle)
            }

            val middle = Swatch(max(sw.x1, x1), max(sw.y1, y1), min(sw.x2, x2), min(sw.y2, y2), timesCovered + 1)
            children.add(middle)

            if (sw.y2 < y2) {
                val bottomMiddle = Swatch(max(sw.x1, x1), sw.y2, min(sw.x2, x2), y2, timesCovered)
                children.add(bottomMiddle)
            }

            if (sw.x2 < x2) {
                val right = Swatch(sw.x2, y1, x2, y2, timesCovered)
                children.add(right)
            }
        }

        fun totalCoveredTwice(): Int =
            if (children.size == 0) {
                if (timesCovered >= 2)
                    (x2 - x1) * (y2 - y1)
                else
                    0
            } else
                children.sumBy { it.totalCoveredTwice() }

        fun totalOverlaps(): Int =
            if (children.size == 0)
                max((timesCovered - 1), 0) * (x2 - x1) * (y2 - y1)
            else
                children.sumBy { it.totalOverlaps() }

        fun totalArea(): Int =
            if (children.size == 0)
                (x2 - x1) * (y2 - y1)
            else
                children.sumBy { it.totalArea() }

        fun totalCovered(): Int =
            if (children.size == 0) {
                if (timesCovered >= 1)
                    (x2 - x1) * (y2 - y1)
                else
                    0
            } else
                children.sumBy { it.totalCovered() }

        fun totalCoveredOnce(): Int =
            if (children.size == 0) {
                if (timesCovered == 1)
                    (x2 - x1) * (y2 - y1)
                else
                    0
            } else
                children.sumBy { it.totalCoveredOnce() }

        fun totalNotCovered(): Int =
            if (children.size == 0) {
                if (timesCovered == 0)
                    (x2 - x1) * (y2 - y1)
                else
                    0
            } else
                children.sumBy { it.totalNotCovered() }

        fun isNotOverlapped(sw: Swatch): Boolean {
            if (sw.x1 >= x2 || sw.y1 >= y2 || sw.x2 <= x1 || sw.y2 <= y1)
                return true

            return if (children.size == 0) {
                (timesCovered <= 1)
            } else {
                !children.any { !it.isNotOverlapped(sw) }
            }
        }
    }

    /*
    val testInput = listOf(
//    "#1 @ 0,0: 100x100",
//    "#2 @ 50,0: 100x100",
//    "#3 @ 0,50: 100x100",
//    "#5 @ 400,400: 200x200",
//    "#4 @ 500,500: 10x10"
//    )

        "#1 @ 1,3: 4x4",
        "#2 @ 3,1: 4x4",
        "#3 @ 5,5: 2x2")
        */

    private lateinit var topSheet: Swatch
    private val allSwatches = mutableMapOf<Int, Day3.Swatch>()

    override fun part1(): Any? {
        val matcher = """#(\d+) @ (\d+),(\d+): (\d+)x(\d+)""".toRegex()

        topSheet = Swatch(0, 0, 1000, 1000, 0)
        allSwatches.clear()
//        testInput.forEach { line ->
        rawInput.forEach { line ->
            val (id, x1, y1, w, h) = matcher.matchEntire(line)!!.groupValues.drop(1).map {it.toInt()}
            val newSwatch = Day3.Swatch(x1, y1, x1 + w, y1 + h, 1)
            allSwatches[id] = newSwatch
        }


        for (swatch in allSwatches.values) {
//            println("splitting by: $swatch")
            topSheet.splitBy(swatch)
        }
//    println(topSheet)
//        println("total area: ${topSheet.totalArea()}")
//        println("total covered area: ${topSheet.totalCovered()}")
//        println("total not covered: ${topSheet.totalNotCovered()}")
//        println("total covered once: ${topSheet.totalCoveredOnce()}")
        println("total covered at least twice: ${topSheet.totalCoveredTwice()}")
//        println("total overlaps: ${topSheet.totalOverlaps()}")

        return topSheet.totalCoveredTwice()
    }

    override fun part2(): Any? {
        for ((id, swatch) in allSwatches) {
            if (topSheet.isNotOverlapped(swatch)) {
                println("found ok swatch: $id")
                return id
            }
        }
        return null
    }
}