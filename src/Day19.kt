typealias Operation = (LongArray, Int, Int, Int) -> LongArray

class Day19(rawInput: List<String>) : Day(rawInput) {

    //seti 1 2 5
    val program = rawInput.drop(1).map { it.split(" ") }
    val ipRegister = rawInput.first().removePrefix("#ip ").toInt()

    val operations = listOf<Operation>(
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and b.toLong()         ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  b.toLong()         ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a)                        ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, a.toLong()                    ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a > get(b))       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > b)       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > get(b))  1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a.toLong() == get(b)) 1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == b.toLong()) 1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == get(b)) 1 else 0) } })

    val allOperations = "addr addi mulr muli banr bani borr bori setr seti gtir gtri gtrr eqir eqri eqrr"
    val opMap: Map<String, Operation> = allOperations.split(" ").zip(operations).toMap()

    private fun step(registers: LongArray, ip: Long): LongArray {
        val (instruction, a, b, c) = program[ip.toInt()]
        return opMap[instruction]!!.invoke(registers, a.toInt(), b.toInt(), c.toInt())
    }

    override fun part1(): Any? {
        var registers = LongArray(6)
        var ip = 0L

        while (ip < program.size) {
            registers[ipRegister] = ip
            registers = step(registers, ip)
            ip = registers[ipRegister] + 1
        }
        return registers[0]
    }

    // NOTE: manually disassembled program: sum all divisors of r4
    private fun runIt(r4: Long) {
        var r0 = 0L
        var r5 = 1L

        do {
            var r2 = 1L

            do {
                if (r5 * r2 == r4)
                    r0 += r5

                r2++
            } while (r2 <= r4)

            r5++
        } while (r5 <= r4)
    }

    override fun part2(): Any? {
        var registers = LongArray(6).apply { set(0, 1) }
        var ip = 0L

        for (i in 1 .. 100) {
            if (ip >= program.size)
                break
            registers[ipRegister] = ip
            println(registers.toList())
            registers = step(registers, ip)
            ip = registers[ipRegister] + 1
        }
        // NOTE: answer = runIt(10551306) by inspection

        return (1..10551306).filter { 10551306 % it == 0 }.sum()
    }
}