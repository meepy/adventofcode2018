
import java.io.File
import kotlin.system.measureTimeMillis

fun measure(action: () -> Unit) {
    val time = measureTimeMillis(action)
    println("time: $time")
}

fun measure10(action: () -> Any?) {
    var time = 0L
    for (i in 1..10) {
        time += measureTimeMillis {
            action()
        }
    }
    println("time: ${time / 10f}")
}

fun main(args: Array<String>) {
    val rawInput = File("inputs/input2019-3.txt").readLines()

    val day: Day = Day2019_4(rawInput)
    measure {println("part1: ${day.part1()}")}
    measure {println("part2: ${day.part2()}")}
//    measure10(day::part1)
//    measure10(day::part2)
}