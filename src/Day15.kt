import kotlin.math.abs

class Day15(rawInput: List<String>) : Day(rawInput) {

    val testData = rawInput
//"""#########
//#G......#
//#.E.#...#
//#..##..G#
//#...##..#
//#...#...#
//#.G...G.#
//#.....G.#
//#########""".lines()

    val maxX = testData.first().length
    val maxY = testData.size

    var units = mutableListOf<Entity>()
    var grid = Array(maxY) { BooleanArray(maxX) } // for path finding only
    var elfHit = 3

    private fun resetData() {
        grid = Array(maxY) { BooleanArray(maxX) } // for path finding only
        units = mutableListOf()

        for ((y, line) in testData.withIndex()) {
            for ((x, char) in line.withIndex()) {
                when (char) {
                    '.' -> grid[y][x] = true
                    'E' -> units.add(Entity(x, y, 200, true))
                    'G' -> units.add(Entity(x, y, 200, false))
                }
            }
        }
    }

    init {
        resetData()
    }

    data class Entity(var x: Int, var y: Int, var hitpoint: Int,
                      val isElf: Boolean, var isDead: Boolean = false) {
        fun dist(x: Int, y: Int): Int {
            return abs(x - this.x) + abs(y - this.y)
        }
    }

    private fun adjacentEmpty(x: Int, y: Int): List<Pair<Int, Int>> {
        return listOf(x to y - 1, x - 1 to y, x + 1 to y, x to y + 1)
            .filter { grid[it.second][it.first] }
    }

    // find newBest, dX, dY
    private fun findBestPath(a: Pair<Int, Int>, b: Pair<Int, Int>): Triple<Int, Int, Int>? {
        var newBest = Integer.MAX_VALUE
        var newDX = 2
        var newDY = 2

        for ((x, y) in adjacentEmpty(a.first, a.second)) {
            val checked = mutableSetOf(a)
            var toCheck = mutableMapOf<Pair<Int, Int>, Triple<Int, Int, Int>>()
            toCheck[x to y] = Triple(1, x - a.first, y - a.second)


            while (!toCheck.isEmpty()) {
                val nextToCheck = mutableMapOf<Pair<Int, Int>, Triple<Int, Int, Int>>()
                for (entry in toCheck) {
                    checked.add(entry.key)

                    val (x, y) = entry.key
                    val (currDist, dX, dY) = entry.value
                    if (currDist > newBest)
                        continue

                    if (abs(x - b.first) + abs(y - b.second) == 0) {
                        if (currDist == newBest) {
                            if (dY * 2 + dX < newDY * 2 + newDX) {
                                newDX = dX
                                newDY = dY
                            }
                        } else {
                            newBest = currDist
                            newDX = dX
                            newDY = dY
                        }
                        continue
                    }

                    for (position in adjacentEmpty(x, y)) {
                        if (!checked.contains(position)) {
                            nextToCheck[position] = Triple(currDist + 1, dX, dY)
                        }
                    }
                }
                toCheck = nextToCheck
            }
        }

        if (newBest == Integer.MAX_VALUE)
            return null
        return Triple(newBest, newDX, newDY)
    }

    private fun round(): Int {
        units.sortBy { it.y * maxX + it.x }

        var totalMoved = 0
        for (entity in units) {
            if (entity.isDead)
                continue

            val targets = units.filter { it.isElf != entity.isElf && !it.isDead }
            if (targets.isEmpty())
                return totalMoved

            val positionsInRange = targets.flatMap { adjacentEmpty(it.x, it.y) }.toSet()
            val isInRange = targets.filter { entity.dist(it.x, it.y) == 1 }

            if (isInRange.isEmpty() && positionsInRange.isEmpty())
                continue

            // move
            if (isInRange.isEmpty()) {
                val best = positionsInRange.toList()
                    .sortedBy { it.second * maxX + it.first }
                    .mapNotNull {
                        val result = findBestPath(entity.x to entity.y, it)
                        if (result == null)
                            null
                        else {
                            result to it
                        }
                    }
                    .minWith(Comparator { p1, p2 ->
                        val (dist1, dX1, dY1) = p1.first
                        val (dist2, dX2, dY2) = p2.first
                        when {
                            dist1 > dist2 -> 1
                            dist1 == dist2  -> {
                                val (x1, y1) = p1.second
                                val (x2, y2) = p2.second
                                (y1 * maxX + x1).compareTo(y2 * maxX + x2)
                            }
                            else -> -1
                        }
                    })?.first

                if (best != null) {
                    grid[entity.y][entity.x] = true
                    entity.x += best.second
                    entity.y += best.third
                    grid[entity.y][entity.x] = false
                    totalMoved++
                }
            }

            // attack
            val isInRange2 = targets.filter { entity.dist(it.x, it.y) == 1 }
            if (isInRange2.isNotEmpty()) {
                val target = isInRange2.minWith(Comparator { p1, p2 ->
                    when {
                        p1.hitpoint > p2.hitpoint -> 1
                        p1.hitpoint == p2.hitpoint -> {
                            (p1.y * maxX + p1.x).compareTo(p2.y * maxX + p2.x)
                        }
                        else -> -1
                    }
                })!!
                if (target.isElf)
                    target.hitpoint -= 3
                else
                    target.hitpoint -= elfHit

                if (target.hitpoint <= 0) {
                    if (target.isElf)
                        println("elf died")
                    target.isDead = true
                    grid[target.y][target.x] = true
                }
            }
        }
        return totalMoved
    }

    private fun printMap() {
        val elfPos = units.filter { it.isElf }.map { it.x to it.y }.toSet()
        val goblinPos = units.filter { !it.isElf }.map { it.x to it.y }.toSet()
        for ((y, line) in grid.withIndex()) {
            var toPrint = ""
            for ((x, isOpen) in line.withIndex()) {
                if (isOpen) {
                    toPrint += "."
                } else if (elfPos.contains(x to y)) {
                    toPrint += "E"
                } else if (goblinPos.contains(x to y)) {
                    toPrint += "G"
                } else {
                    toPrint += "#"
                }
            }
            println(toPrint)
        }
    }

    override fun part1(): Any? {
        printMap()
        var roundNum = 0
        while ( units.count { it.isElf } != 0 && units.count { !it.isElf } != 0) {
            val moved = round()
            units.removeIf { it.isDead }
            roundNum++
//            printMap()
        }
        return "${roundNum - 1}, ${units.sumBy { it.hitpoint }}"
    }

    override fun part2(): Any? {
        for (i in 4..25) { // just look at the output to find the first one
            resetData()
            elfHit = i

            var roundNum = 0
            while ( units.count { it.isElf } != 0 &&
                units.count { !it.isElf } != 0) {
                round()
                units.removeIf { it.isDead }
                roundNum++
            }
            println("${roundNum - 1}, ${units.sumBy { it.hitpoint }}")

        }
        return null
    }
}