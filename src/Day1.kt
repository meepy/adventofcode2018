class Day1(rawInput: List<String>) : Day(rawInput) {

    override fun part1(): Any? {

        val total = rawInput.map(String::toInt).sum()

        return total
    }

    override fun part2(): Any? {
        val mappedLines = rawInput.map(String::toInt)

        val reached = mutableSetOf<Int>()

        var total = 0
        found@ while (true) {
            for (i in mappedLines) {
                total += i
                if (reached.contains(total)) {
                    break@found
                }
                reached.add(total)
            }
        }

        return total
    }
}