import java.util.*
import kotlin.math.abs

typealias Point = Pair<Int, Int>  // y, x

class Day20(rawInput: List<String>) : Day(rawInput) {

    val mockInput = rawInput
//    listOf("^WW(N(NE)(E(S|E)))S$")
//    listOf("^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$")
//    listOf("^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$")
//    listOf("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$")
//    listOf("^ENWWW(NEEE|SSE(EE|N))$")
//    listOf("^(N|S)(N|S)(N|S)(N|S)$")
//    listOf("^WWWW(SSSSS|)WWWWW$")
//    listOf("^(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)(N|S|E|W)$")

    val chart = mutableSetOf<Point>()

    private fun printChart(visited: Set<Point> = setOf()) {
        val minY = chart.minBy { it.first }!!.first
        val maxY = chart.maxBy { it.first }!!.first

        val minX = chart.minBy { it.second }!!.second
        val maxX = chart.maxBy { it.second }!!.second

        for (y in minY - 1 .. maxY + 1) {
            println((minX - 1 .. maxX + 1).joinToString(separator = "") { x ->
                when {
                    visited.contains(Pair(y, x)) ->
                        "X"
                    !chart.contains(Pair(y, x)) ->
                        "#"
                    abs(x) % 2 == 0 && abs(y) % 2 == 0 ->
                        "."
                    abs(x) % 2 == 1 && abs(y) % 2 == 0 ->
                        "|"
                    abs(x) % 2 == 0 && abs(y) % 2 == 1 ->
                        "-"
                    else ->
                        throw Exception("invalid value in chart: $x, $y")
                }
            })
        }
    }

    private fun adjacent(visited: Set<Point>, point: Pair<Int, Int>) = sequence {
        val (y, x) = point
        if (chart.contains(Pair(y, x - 1)) && !visited.contains(Pair(y, x - 1)))
            yield(Pair(y, x - 1))
        if (chart.contains(Pair(y, x + 1)) && !visited.contains(Pair(y, x + 1)))
            yield(Pair(y, x + 1))
        if (chart.contains(Pair(y - 1, x)) && !visited.contains(Pair(y - 1, x)))
            yield(Pair(y - 1, x))
        if (chart.contains(Pair(y + 1, x)) && !visited.contains(Pair(y + 1  , x)))
            yield(Pair(y + 1, x))
    }

    private fun findMaxDistances(): Triple<Set<Point>, Int, Int> {
        val visited = mutableSetOf<Point>().apply { add(Pair(0, 0)) }
        var currentNodes = mutableSetOf<Point>().apply { add(Pair(0, 0)) }
        var distance = 0
        var totalOver100 = 0
        while (currentNodes.isNotEmpty()) {
            if (distance % 2 == 0 && distance / 2 >= 1000)
                totalOver100 += currentNodes.size

            val nextNodes = mutableSetOf<Point>()
            for (point in currentNodes) {
                val newNodes = adjacent(visited, point).toList()
                nextNodes.addAll(newNodes)
                visited.addAll(newNodes)
            }
            currentNodes = nextNodes
            distance += 1
        }
        return Triple(visited, distance / 2, totalOver100)
    }

    override fun part1(): Any? {
        val input = mockInput.first()
        val paths = ArrayDeque<Set<Pair<Int, Int>>>().apply {
            chart.add(Pair(0, 0))
            push(setOf(Pair(0, 0)))
        }

        var index = 0
        while (paths.isNotEmpty()) {
            index += 1
            val positions = paths.pop()
            when (input[index]) {
                'W' -> {
                    val newSet = mutableSetOf<Point>()
                    for ((y, x) in positions) {
                        chart.add(Pair(y, x - 1))
                        chart.add(Pair(y, x - 2))
                        newSet.add(Pair(y, x - 2))
                    }
                    paths.push(newSet)
                }
                'E' -> {
                    val newSet = mutableSetOf<Point>()
                    for ((y, x) in positions) {
                        chart.add(Pair(y, x + 1))
                        chart.add(Pair(y, x + 2))
                        newSet.add(Pair(y, x + 2))
                    }
                    paths.push(newSet)
                }
                'N' -> {
                    val newSet = mutableSetOf<Point>()
                    for ((y, x) in positions) {
                        chart.add(Pair(y - 1, x))
                        chart.add(Pair(y - 2, x))
                        newSet.add(Pair(y - 2, x))
                    }
                    paths.push(newSet)
                }
                'S' -> {
                    val newSet = mutableSetOf<Point>()
                    for ((y, x) in positions) {
                        chart.add(Pair(y + 1, x))
                        chart.add(Pair(y + 2, x))
                        newSet.add(Pair(y + 2, x))
                    }
                    paths.push(newSet)
                }
                '(' -> {
                    paths.push(positions) // original paths
                    paths.push(setOf())   // new paths
                    paths.push(positions) // current
                }
                ')' -> {
                    val alternates = paths.pop().toMutableSet() // new paths
                    alternates.addAll(positions)
                    paths.pop() // original paths
                    paths.push(alternates)
                }
                '|' -> {
                    val alternates = paths.pop().toMutableSet() // new paths
                    alternates.addAll(positions)
                    val original = paths.peek() // original paths
                    paths.push(alternates)
                    paths.push(original)
                }
                else ->
                    Unit
            }
        }


        val (visited, max, total) = findMaxDistances()
        //printChart(visited)
        return max
    }

    override fun part2(): Any? {
        val (visited, max, total) = findMaxDistances()
        return total
    }
}