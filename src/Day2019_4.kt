import Day2019_4.Globals.end
import Day2019_4.Globals.start

/*
--- Day 4: Secure Container --- You arrive at the Venus fuel depot only to
discover it's protected by a password. The Elves had written the password on a
sticky note, but someone threw it out.

However, they do remember a few key facts about the password:

It is a six-digit number.  The value is within the range given in your puzzle
input.  Two adjacent digits are the same (like 22 in 122345).  Going from left
to right, the digits never decrease; they only ever increase or stay the same
(like 111123 or 135679).  Other than the range rule, the following are true:

111111 meets these criteria (double 11, never decreases).  223450 does not meet
these criteria (decreasing pair of digits 50).  123789 does not meet these
criteria (no double).  How many different passwords within the range given in
your puzzle input meet these criteria?

Your puzzle input is 246515-739105.

--- Part Two ---

An Elf just remembered one more important detail: the two adjacent matching
digits are not part of a larger group of matching digits.

Given this additional criterion, but still ignoring the range rule, the
following are now true:

112233 meets these criteria because the digits never decrease and all repeated
digits are exactly two digits long.  123444 no longer meets the criteria (the
repeated 44 is part of a larger group of 444).  111122 meets the criteria (even
though 1 is repeated more than twice, it still contains a double 22).  How many
different passwords within the range given in your puzzle input meet all of the
criteria?

*/


class Day2019_4(rawInput: List<String>) : Day(rawInput) {

    object Globals {
//        const val start = 246515
        const val start = 246666
//        const val end = 739105
        const val end = 699999 + 1
    }

    private fun checkNumber(num: Int): Boolean {
        val str = num.toString()
        var leastDigit = 1
        for (char in str) {
            if (char.toInt() < leastDigit)
                return false
            leastDigit = char.toInt()
        }
        var lastChar = '0'
        for (char in str) {
            if (char == lastChar)
                return true
            lastChar = char
        }
        return false
    }

    private fun checkNumber2(num: Int): Boolean {
        val str = num.toString()
        var leastDigit = 1
        for (char in str) {
            if (char.toInt() < leastDigit)
                return false
            leastDigit = char.toInt()
        }

        var numDoubles = 0
        var lastFound = 0
        var lastChar = '0'

        for (char in str) {
            if (char == lastChar) {
                lastFound++
            } else {
                if (lastFound == 1)
                    numDoubles++
                lastFound = 0
            }
            lastChar = char
        }

        if (lastFound == 1)
            numDoubles++

        return numDoubles >= 1
    }

    override fun part1(): Any? {
        var total = 0
        for (num in start until end) {
            if (checkNumber(num))
                total += 1
        }
        return total // 1048
    }

    override fun part2(): Any? {
        var total = 0
        for (num in start until end) {
            if (checkNumber2(num))
                total += 1
        }
        return total // 677
    }
}
