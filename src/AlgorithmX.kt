class AlgorithmX(rawInput: List<String>) : Day(rawInput) {
    override fun part1(): Any? {
        return "not implemented"
    }

    val data1 = mutableListOf<Triple<IntArray, IntArray, IntArray>>()
    var data2: List<IntArray>

    init {
        var i = 0
        while (rawInput[i].isNotEmpty()) {
            val (a, b, c) = rawInput.subList(i, i + 3)

            val before = a.removePrefix("Before: [").removeSuffix("]")
                .split(", ").map { it.toInt() }
            val operation = b
                .split(" ").map { it.toInt() }
            val after = c.removePrefix("After:  [").removeSuffix("]")
                .split(", ").map { it.toInt() }

            data1.add(Triple(operation.toIntArray(), before.toIntArray(), after.toIntArray()))
            i += 4
        }

        data2 = rawInput.drop(i + 2).map { line ->
            line.split(" ").map { it.toInt() }
                .toIntArray()
        }
    }


    val operations = listOf<(IntArray, Int, Int, Int) -> IntArray>(
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a)                        ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, a                             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a > get(b))       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > b)       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > get(b))  1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a == get(b))      1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == b)      1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == get(b)) 1 else 0) } })

    private fun findMatches() = data1.asSequence().map { (byteCode, before, after) ->
        val (op, a, b, c) = byteCode
        op to operations.withIndex().filter {
            it.value(before, a, b, c).contentEquals(after)
        }.map { it.index }
    }

    private fun findOpcodes(): Map<Int, Set<Int>> {
        val opcodeMap = mutableMapOf<Int, MutableSet<Int>>()
        for ((opCode, matches) in findMatches()) {
            if (matches.isEmpty())
                throw Exception("bad input")
            opcodeMap.getOrPut(opCode) { mutableSetOf() }.addAll(matches)
        }

        return opcodeMap
    }

    private fun checkSolution(solution: Map<Int, Int>) {
        println(solution.toSortedMap())
        for ((byteCode, before, after) in data1) {
            val (op, a, b, c) = byteCode
            val operation = operations[solution[op]!!]
            if (!operation(before, a, b, c).contentEquals(after)) {
                throw Exception("opcode solution failed")
            }
        }
    }

    private fun solve(choices: List<Choice>): MutableMap<Int, Int>? {
        if (choices.isEmpty()) // all constraints satisfied
            return mutableMapOf()

        val sums = IntArray(choices.first().constraints.size)
        for (row in choices) {
            for (entry in row.constraints.withIndex()) {
                if (entry.value)
                    sums[entry.index] += 1
            }
        }

        // pick lowest served constraint
        val pick = sums.withIndex().minBy { it.value }!!.index
        if (sums[pick] == 0)
            return null

        for (row in choices.filter { it.constraints[pick] }) {
            // remove all columns in row and any rows with those columns
            val remaining = mutableListOf<Choice>()

            val columnsToCopy = row.constraints.withIndex().filter { !it.value }.map { it.index }
            val columnsToRemove = row.constraints.withIndex().filter { it.value }.map { it.index }
            for (toCopy in choices.filter { choice -> columnsToRemove.none { choice.constraints[it] } }) {
                val newRow = Choice(toCopy.opcode, toCopy.opIndex)
                newRow.constraints = BooleanArray(columnsToCopy.size)
                columnsToCopy.forEachIndexed { i, value -> newRow.constraints[i] = toCopy.constraints[value] }
                remaining.add(newRow)
            }

            val solution = solve(remaining)
            if (solution != null) {
                return solution.apply { put(row.opcode, row.opIndex) }
            }
        }
        return null
    }

    data class Choice(val opcode: Int, val opIndex: Int) {
        // constraints:
        //     0..15: each operation has one opcode
        //     16..31: each opcode has one operation
        lateinit var constraints: BooleanArray
    }

    private fun buildChoices(opcodeMap: Map<Int, Set<Int>>): List<Choice> {
        val choices = mutableListOf<Choice>()
        for ((opcode, operations) in opcodeMap) {
            for (opIndex in operations) {
                val newChoice = Choice(opcode, opIndex)
                newChoice.constraints = BooleanArray(32).apply {
                    set(opcode, true)
                    set(opIndex + 16, true)
                }
                choices.add(newChoice)
            }
        }
        check(choices.size == opcodeMap.toList().sumBy { it.second.count() })

        return choices
    }

    override fun part2(): Any? {
        // opcode to operation index
        val opcodeMap = findOpcodes()
        println(opcodeMap)

        val choices = buildChoices(opcodeMap)
        val solution = solve(choices)!!

        checkSolution(solution)

        var registers = IntArray(4)
        data2.forEach { (op, a, b, c) ->
            registers = operations[solution[op]!!](registers, a, b, c)
        }
        return registers.first()
    }
}