import java.util.*

class Day8(rawInput: List<String>) : Day(rawInput) {
    companion object {
        lateinit var input: IntArray
        var valueSum = 0
    }

    init {
        input = rawInput.first().split(" ").map { it.toInt() }.toIntArray()
    }

    data class Node(val index: Int, val numChildren: Int, val numValues: Int) {
        val children = mutableListOf<Node>()
        var value = 0
        var size = 2

        constructor(index: Int) : this(index, input[index], input[index + 1])

        fun buildValues() {
            size += children.sumBy { it.size }
            for (i in 0 until numValues) {
                val v = input[index + size + i]
                if (children.size == 0)
                    value += v
                else if (v > 0 && v <= children.size)
                    value += children[v - 1].value
                valueSum += v
            }
            size += numValues
        }
    }

    val root = Node(0)

    private fun buildTree() {
        val nodeStack = Stack<Node>().apply { push(root) }
        while (nodeStack.isNotEmpty()) {
            val top = nodeStack.peek()
            if (top.children.size == top.numChildren) {
                nodeStack.pop().buildValues()
            } else {
                val topSize = top.size + top.children.sumBy { it.size }
                val newChild = Node(top.index + topSize)
                top.children.add(newChild)
                nodeStack.push(newChild)
            }
        }
    }

    override fun part1(): Any? {
        buildTree()
        return valueSum
    }

    override fun part2(): Any? {
        return root.value
    }
}