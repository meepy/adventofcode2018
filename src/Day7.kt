import java.util.*

class Day7(rawInput: List<String>) : Day(rawInput) {

    private val preRequisites = mutableMapOf<Char, MutableSet<Char>>()

    private fun initData() {
        preRequisites.clear()
        rawInput.forEach {
            val (a, b) = """Step ([A-Z]) must be finished before step ([A-Z]) can begin.""".toRegex().matchEntire(it)?.destructured
                ?: throw Exception("bad input")

            preRequisites.getOrPut(b.first()) { mutableSetOf() }.add(a.first())
            preRequisites.putIfAbsent(a.first(), mutableSetOf())
        }
    }

    private fun findOrderedTopologicalSort(): String {
        val found = Stack<Char>()

        while (preRequisites.isNotEmpty()) {
            val nextStep = preRequisites.filter { it.value.isEmpty() }.keys.min()!!
            preRequisites.run {
                remove(nextStep)
                values.forEach {
                    it.remove(nextStep)
                }
            }
            found.push(nextStep)
        }

        return found.joinToString("")
    }

    // all workers are elves with a task and finish time
    data class Elf(var task: Char, var finish: Int)

    private fun findOrderedWorkerSort(): Int {
        var currentTime = -1
        val workers = (1..5).map { Elf(' ', 0) }

        while (preRequisites.isNotEmpty()) {

            // go to next time
            currentTime = workers.filter { it.finish > currentTime }.map { it.finish }.min()!!

            // remove finished tasks
            workers.filter { it.finish == currentTime }.forEach { elf ->
                preRequisites.forEach {
                    it.value.remove(elf.task)
                }
            }

            // find remaining possibilities
            val possibilities = preRequisites.filter { it.value.size == 0 }.keys.sorted()

            // allocate new tasks
            var taskIndex = 0
            workers.filter { it.finish <= currentTime }.forEach { elf ->
                if (taskIndex < possibilities.size) {
                    val nextStep = possibilities[taskIndex]
                    preRequisites.remove(nextStep)
                    taskIndex++

                    elf.task = nextStep
                    elf.finish = currentTime + 60 + (nextStep - 'A' + 1)
                }
            }
        }

        return workers.map { it.finish }.max()!!
    }

    override fun part1(): Any? {
        initData()
        return findOrderedTopologicalSort()
    }

    override fun part2(): Any? {
        initData()
        return findOrderedWorkerSort()
    }
}