class Day11(rawInput: List<String>) : Day(rawInput) {
    val serialNum = 6392

    private fun compute(x: Int, y: Int): Int {
        val rackID = x + 10
        var power = rackID * y + serialNum
        power *= rackID

        return (power / 100) % 10 - 5
    }

    override fun part1(): Any? {
        var bestX = 0
        var bestY = 0
        var bestTotal = 0

        for (x in 1 .. (300 - 2)) {
            for (y in 1 .. (300 - 2)) {
                val total = (0..2).map { x2 ->
                    (0..2).map { y2 ->
                        compute(x + x2, y + y2)
                    }.sum()
                }.sum()
                if (total > bestTotal) {
                    bestX = x
                    bestY = y
                    bestTotal = total
                }
            }
        }
        return "total: $bestTotal, ($bestX, $bestY)"
    }

    private fun findBest(size: Int, arr: Array<IntArray>): List<Int>{
        var bestX = 0
        var bestY = 0
        var bestTotal = 0
        for (x in 0 .. 300 - size) {
            for (y in 0 .. 300 - size) {
                if (arr[x][y] > bestTotal) {
                    bestX = x + 1
                    bestY = y + 1
                    bestTotal = arr[x][y]
                }
            }
        }
        return listOf(bestX, bestY, bestTotal)
    }

    override fun part2(): Any? {

        val initialArray = Array(300) { x ->
            IntArray(300) { y ->
                compute(x + 1, y + 1)
            }
        }

        val accumulator = with(initialArray) { Array(300) {get(it).clone()} }

        var bestSize = 1
        var currBest = findBest(bestSize, accumulator)
//        println("total: ${currBest[2]}, size: $bestSize, (${currBest[0]}, ${currBest[1]})")

        for (size in 2 .. 300) {
            for (x in 1 .. (300 - size + 1)) {
                for (y in 1 .. (300 - size + 1)) {
                    for (x2 in x .. (x + size - 2))
                        accumulator[x - 1][y - 1] += initialArray[x2 - 1][y + size - 2]
                    for (y2 in y .. (y + size - 2))
                        accumulator[x - 1][y - 1] += initialArray[x + size - 2][y2 - 1]
                    accumulator[x - 1][y - 1] += initialArray[x + size - 2][y + size - 2]
                }
            }
            val result = findBest(size, accumulator)
            if (result[2] > currBest[2]) {
                currBest = result
                bestSize = size
            }
//            println("total: ${currBest[2]}, size: $bestSize, (${currBest[0]}, ${currBest[1]})")
        }

        return "${currBest[0]},${currBest[1]},$bestSize"
    }
}