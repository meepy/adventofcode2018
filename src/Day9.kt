import java.util.*

class Day9(rawInput: List<String>) : Day(rawInput) {
    var numPlayers = 0
    var lastMarble = 0

    init {
        val parser = """(\d+) players; last marble is worth (\d+) points""".toRegex()
        val (x, y) = parser.matchEntire(rawInput.first())?.destructured  ?: throw Exception("bad input")

        numPlayers = x.toInt()
        lastMarble = y.toInt()

//        numPlayers = 9
//        lastMarble = 25
    }

    val players = LongArray(numPlayers)

    private fun playGameSmall() {
        val marbles = mutableListOf(0)
        var currIndex = 0

        for (i in 1..lastMarble) {
            val toPlay = (i - 1) % numPlayers
            if (i % 11 == 0) {
                val toRemove = (marbles.size + currIndex - 7) % marbles.size
                players[toPlay] += marbles.removeAt(toRemove).toLong() + i
                currIndex = toRemove % marbles.size
            } else {
                currIndex = (currIndex + 2) % marbles.size
                marbles.add(currIndex, i)
            }
//            println("[$i] ${marbles.joinToString(separator = " ") { if (it == marbles[currIndex]) "($it)" else "$it" }}")
        }
    }

    private fun goRight(steps: Int, iterator: MutableListIterator<Int>, list: LinkedList<Int>): MutableListIterator<Int> {
        var result = iterator
        for (i in 1..steps) {
            if (!result.hasNext())
                result = list.listIterator(0)
            result.next()
        }
        return result
    }

    private fun goLeft(steps: Int, iterator: MutableListIterator<Int>, list: LinkedList<Int>): Pair<MutableListIterator<Int>, Int> {
        var result = iterator
        var score = 0
        for (i in 1..steps) {
            if (!result.hasPrevious())
                result = list.listIterator(list.size)
            score = result.previous()
        }
        return result to score
    }

    private fun playGameLarge() {
        val marbles = LinkedList<Int>().apply { add(0) } // NOTE: rotating an ArrayDeque is better
        var currIndex = marbles.listIterator(0)

        for (i in 1..lastMarble) {
            val toPlay = (i - 1) % numPlayers
            if (i % 23 == 0) {
                val (nextIndex, score) = goLeft(8, currIndex, marbles)
//                println("score: $i + $score")
                players[toPlay] += score.toLong() + i

                currIndex = nextIndex
                currIndex.remove()
                currIndex = goRight(1, currIndex, marbles)
            } else {
                currIndex = goRight(1, currIndex, marbles)
                currIndex.add(i)
            }
//            val marble = if (currIndex.hasPrevious()) marbles[currIndex.previousIndex()] else marbles.last()
//            println("[$i] ${marbles.joinToString(separator = " ") { if (it == marble) "($it)" else "$it" }}")
        }
    }

    override fun part1(): Any? {
        players.fill(0)
        playGameSmall()
        return players.max()
    }

    override fun part2(): Any? {
        players.fill(0)
        lastMarble *= 100
        playGameLarge()
        return players.max()
    }
}