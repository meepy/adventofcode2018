class Day21(rawInput: List<String>) : Day(rawInput) {

    val program = rawInput.drop(1).map { it.split(" ") }.toMutableList()
    val ipRegister = rawInput.first().removePrefix("#ip ").toInt()

    var terminate = false
    val seen = mutableSetOf<Long>()
    var answer = 0L

    val operations = listOf<Operation>(
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and b.toLong()         ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  b.toLong()         ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a)                        ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, a.toLong()                    ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a > get(b))       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > b)       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > get(b))  1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a.toLong() == get(b)) 1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == b.toLong()) 1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == get(b)) 1 else 0) } })

    val allOperations = "addr addi mulr muli banr bani borr bori setr seti gtir gtri gtrr eqir eqri eqrr"
    val opMap = allOperations.split(" ").zip(operations).toMap().toMutableMap()

    private fun step(registers: LongArray, ip: Long): LongArray {
        val (instruction, a, b, c) = program[ip.toInt()]
        return opMap[instruction]!!.invoke(registers, a.toInt(), b.toInt(), c.toInt())
    }

    private fun runAndHalt() {
        var registers = LongArray(6)
        var ip = 0L

        while (ip < program.size && !terminate) {
            registers[ipRegister] = ip
            registers = step(registers, ip)
            ip = registers[ipRegister] + 1
        }
    }

    override fun part1(): Any? {
        opMap["eqrr"] = { r, a, b, c -> r.copyOf().apply {
            terminate = true
            answer = get(1)
        } }
        runAndHalt()
        return answer
    }

    override fun part2(): Any? {
        opMap["eqrr"] = { r, a, b, c -> r.copyOf().apply {
            terminate = !seen.add(get(1))
            if (!terminate)
                answer = get(1)
            set(c, if (get(a) == get(b)) 1 else 0)
        } }
        terminate = false
        runAndHalt()
        return answer
    }
}