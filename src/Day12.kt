import java.util.*

class Day12(rawInput: List<String>) : Day(rawInput) {
    val allPlants = BitSet()
    var initialIndex = -5L
    val productionRules = BooleanArray(32)

    init {
//        initial state: #.#####.##.###...#...#.####..#..#.#....##.###.##...#####.#..##.#..##..#..#.#.#.#....#.####....#..#
        val plantRow = """initial state: ([#\.]+)""".toRegex().matchEntire(rawInput.first())?.groupValues?.get(1) ?:
            throw Exception("bad input: ${rawInput.first()}")

        plantRow.forEachIndexed { index, char ->
            when (char) {
                '#' -> allPlants[index - initialIndex.toInt()] = true
                '.' -> Unit
                else -> throw Exception("bad input: $index, $char")
            }
        }

//        #.#.. => .
        for (line in rawInput.drop(2)) {
            val (rule, result) = """([#\.]+) => ([#\.])""".toRegex().matchEntire(line)?.destructured ?:
                throw Exception("bad input: $line")
            productionRules[positionToIndex(rule, '#')] = (result == "#")
        }
        println(allPlants)
        println(productionRules.toList())

        check(!productionRules[0])
    }

    private fun positionToIndex(arr: String, test: Char): Int {
        var total = 0
        if (arr[4] == test)
            total += 16
        if (arr[3] == test)
            total += 8
        if (arr[2] == test)
            total += 4
        if (arr[1] == test)
            total += 2
        if (arr[0] == test)
            total += 1
        return total
    }

    private fun positionToIndex(a: Boolean, b: Boolean, c: Boolean, d: Boolean, e: Boolean): Int {
        var total = 0
        if (e)
            total += 16
        if (d)
            total += 8
        if (c)
            total += 4
        if (b)
            total += 2
        if (a)
            total += 1
        return total
    }

    private fun oneStep(inSet: BitSet): Pair<BitSet, Long> {

        val newPlants = BitSet()
        val first = inSet.nextSetBit(0) - 4
        var shiftLeft = 0
        if (first > 2)
            shiftLeft = first - 2
        for (i in first until inSet.length()) {
            // NOTE: the 5 gets was faster for some reason
//            val rule = inSet.get(i, i + 5).toLongArray()
//            if (rule.isNotEmpty()) {
                val rule = positionToIndex(inSet.get(i), inSet.get(i + 1), inSet.get(i + 2), inSet.get(i + 3), inSet.get(i + 4))
//                if (productionRules[rule[0].toInt()]) {
                if (productionRules[rule]) {
                    newPlants.set(i + 2 - shiftLeft)
                }
//            }
        }

        return newPlants to shiftLeft.toLong()
    }

    override fun part1(): Any? {
        var currPlants = allPlants
        var myIndex = initialIndex

        for (i in 1 .. 20) {
            val (nextPlants, shiftLeft) = oneStep(currPlants)
            currPlants = nextPlants
            myIndex += shiftLeft

            println(currPlants)
        }

        var total = 0L
        for (i in currPlants.stream()) {
            total += i + myIndex
        }

        return total
    }

    val plantMap = mutableSetOf<BitSet>()
//    val plantSequence = mutableListOf<Pair<BitSet, Long>>()

    override fun part2(): Any? {
        var currPlants = allPlants
        var myIndex = initialIndex

        for (i in 1 .. 50000000000L) {
            val (nextPlants, shiftLeft) = oneStep(currPlants)
            myIndex += shiftLeft

            plantMap.add(currPlants)
//            plantSequence.add(currPlants to shiftLeft)

            if (plantMap.contains(nextPlants)) {
                println("found repeating value at $i, $shiftLeft")

                if (nextPlants.equals(currPlants)) {
                    myIndex += shiftLeft * (50000000000L - i)
                } else {
                    // NOTE: slightly more complicated computation if the repeat isn't this one
                    // 1. find index of nextPlants in plantSequence
                    // 2. sum from index to end
                    // 3. myIndex += sum * ((50000000000L - i) / (plantSequence.size - index))
                    // 4. myIndex += sum (index to index + remainder)
                    // 5. set currPlants to the plantSequence[index + remainder] one
                }
                break
            }

            currPlants = nextPlants
        }

        var total = 0L
        for (i in currPlants.stream()) {
            total += i + myIndex
        }

        return total
    }
}