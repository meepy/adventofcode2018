class Day14(rawInput: List<String>) : Day(rawInput) {

    companion object {
        const val INPUT = 290431
    }
    val recipes = mutableListOf(3, 7)
    var elf1 = 0
    var elf2 = 1

    private fun step() {
        val sum = recipes[elf1] + recipes[elf2]
        if (sum > 9) {
            recipes.add(1)
            recipes.add(sum - 10)
        } else {
            recipes.add(sum)
        }
        elf1 = (elf1 + recipes[elf1] + 1) % recipes.size
        elf2 = (elf2 + recipes[elf2] + 1) % recipes.size
    }

    override fun part1(): Any? {
        while (recipes.size < INPUT + 10) {
            step()
        }

        return recipes.subList(INPUT, INPUT + 10).joinToString(separator = "")
    }

    override fun part2(): Any? {
        while (recipes.size < 100000000) { // NOTE: found by experimentation
            step()
        }

        return recipes.joinToString(separator = "").indexOf(INPUT.toString())
    }
}