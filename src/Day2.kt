class Day2(rawInput: List<String>) : Day(rawInput) {

    var finalresult = ""

    private fun isOffByOne(s1: String, s2: String): Boolean {
        if (s1.length != s2.length)
            return false

        val combined = s1.zip(s2)

        if (combined.count { it.first != it.second } != 1)
            return false

        val result = combined.mapNotNull {
                if (it.first == it.second)
                    it.first
                else null
            }.joinToString("")

        finalresult = "found result: $result"

        return true
    }

    override fun part1(): Any? {
        val counts = IntArray(26)
        var numTwos = 0
        var numThrees = 0

        for (line in rawInput) {
            counts.fill(0)
            for (c in line) {
                counts[c.toInt() - 'a'.toInt()] += 1
            }
            if (counts.contains(2))
                numTwos++
            if (counts.contains(3))
                numThrees++
        }

        return "$numTwos * $numThrees = ${numTwos * numThrees}"
    }

    override fun part2(): Any? {
        for (i in 0 until rawInput.size - 2) {
            for (j in i until rawInput.size - 1) {
                if (isOffByOne(rawInput[i], rawInput[j])) {
                    return finalresult
                }
            }
        }
        return null
    }
}
