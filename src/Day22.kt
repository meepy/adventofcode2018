
import java.util.*
import kotlin.math.abs
import kotlin.math.min

class Day22(rawInput: List<String>) : Day(rawInput) {

//    depth: 3339
//    target: 10,715
    val depth = rawInput.first().removePrefix("depth: ").toInt()
//            510
    val target = rawInput[1].removePrefix("target: ").split(",").map { it.toInt() }
//            listOf(10, 10)

    companion object {
        val chartIndexes = mutableMapOf<Point, Int>()
        val chartErosions = mutableMapOf<Point, Int>()
        val chartTypes = mutableMapOf<Point, Int>()
        val bestTimes = mutableMapOf<Point, Pair<Int, Int>>()
    }

    val maxX = 500 //1005 - target[1] //target[0]
    val maxY = 1000 //1005 - target[0] //target[1]

    init {
        for (x in 0 .. maxX) {
            val geologicIndex = x * 16807
            chartIndexes[Pair(0, x)] = geologicIndex
            chartErosions[Pair(0, x)] = (geologicIndex + depth) % 20183
        }
        for (y in 0 .. maxY) {
            val geologicIndex = y * 48271
            chartIndexes[Pair(y, 0)] = geologicIndex
            chartErosions[Pair(y, 0)] = (geologicIndex + depth) % 20183
        }
        chartIndexes[Pair(0, 0)] = 0
        chartErosions[Pair(0, 0)] = 0
        for (y in 1 .. maxY) {
            for (x in 1 .. maxX) {
                if (y == target[1] && x == target[0]) {
                    chartIndexes[Pair(target[1], target[0])] = 0
                    chartErosions[Pair(target[1], target[0])] = 0
                } else {
                    val geologicIndex = chartErosions[Pair(y - 1, x)]!! * chartErosions[Pair(y, x - 1)]!!
                    chartIndexes[Pair(y, x)] = geologicIndex
                    chartErosions[Pair(y, x)] = (geologicIndex + depth) % 20183
                }
            }
        }

        for (y in 0 .. maxY)
            for (x in 0 .. maxX)
                chartTypes[Pair(y, x)] = chartErosions[Pair(y, x)]!! % 3

        // surrounding walls
        for (x in -1 .. maxX + 1) {
            chartTypes[Pair(-1, x)] = 3
            chartTypes[Pair(maxY + 1, x)] = 3
        }
        for (y in -1 .. maxY + 1) {
            chartTypes[Pair(y, -1)] = 3
            chartTypes[Pair(y, maxX + 1)] = 3
        }

        for (y in -1 .. maxY + 1) {
            for (x in -1 .. maxX + 1) {
                bestTimes[Pair(y, x)] = Pair(Integer.MAX_VALUE, Integer.MAX_VALUE)
            }
        }
        bestTimes[Pair(0, 0)] = Pair(0, 7)
    }

    /*
    0(rocky):  torch, climbing gear
    1(wet):    climbing bear, neither
    2(narrow): torch, neither
     */

    private fun distance(from: Point, to: Point): Pair<Int, Int> {
        val move = chartTypes[from]!! to chartTypes[to]!!
        return when (move) {
            Pair(0, 0) -> Pair(bestTimes[from]!!.first + 1, bestTimes[from]!!.second + 1)
            Pair(0, 1) -> Pair(bestTimes[from]!!.second + 1, bestTimes[from]!!.second + 8)
            Pair(0, 2) -> Pair(bestTimes[from]!!.first + 1, bestTimes[from]!!.first + 8)
            Pair(1, 0) -> Pair(bestTimes[from]!!.first + 8, bestTimes[from]!!.first + 1)
            Pair(1, 1) -> Pair(bestTimes[from]!!.first + 1, bestTimes[from]!!.second + 1)
            Pair(1, 2) -> Pair(bestTimes[from]!!.second + 8, bestTimes[from]!!.second + 1)
            Pair(2, 0) -> Pair(bestTimes[from]!!.first + 1, bestTimes[from]!!.first + 8)
            Pair(2, 1) -> Pair(bestTimes[from]!!.second + 8, bestTimes[from]!!.second + 1)
            Pair(2, 2) -> Pair(bestTimes[from]!!.first + 1, bestTimes[from]!!.second + 1)
            else -> Pair(Integer.MAX_VALUE, Integer.MAX_VALUE)
        }
    }

    private class Vertex(val pt: Point) {
        fun neighbors() = sequence {
            yield(Pair(pt.first - 1, pt.second))
            yield(Pair(pt.first + 1, pt.second))
            yield(Pair(pt.first, pt.second - 1))
            yield(Pair(pt.first, pt.second + 1))
        }
    }

    fun Point.mdist(): Int {
        return abs(this.first - target[1]) + abs(this.second - target[0])
    }

    private fun findTimes() {
        val compareVertex = compareBy<Vertex>( { bestTimes[it.pt]!!.first + it.pt.mdist() },
            { bestTimes[it.pt]!!.second + it.pt.mdist() + 7 },
            { it.pt.first }, { it.pt.second } )

        val considering = TreeSet<Vertex>(compareVertex).apply {
            add(Vertex(Point(0, 0)))
        }

        while (considering.isNotEmpty()) {
            val consider = considering.pollFirst()
            if (consider.pt == Point(target[1], target[0]))
                break

            for (neighbor in consider.neighbors()) {
                val time = distance(consider.pt, neighbor)
                val neighborTime = bestTimes[neighbor]!!
                if (time.first < neighborTime.first || time.second < neighborTime.second) {
                    val newVertex = Vertex(neighbor)
                    considering.remove(newVertex)
                    bestTimes[neighbor] = Pair(min(time.first, neighborTime.first),
                        min(time.second, neighborTime.second))
                    considering.add(newVertex)
                }
            }
        }
    }

    private fun printChart(x1: Int, y1: Int) {
        (0 .. y1).forEach{ y ->
            println((0 .. x1).joinToString(separator = "") { x ->
                when (chartErosions[Pair(y, x)]!! % 3) {
                    0 -> "."
                    1 -> "="
                    else -> "|"
                }
            })
        }
    }

    override fun part1(): Any? {
        println("$depth, $target")

        val total = (0 .. target[1]).sumBy { y ->
            (0 .. target[0]).sumBy { x ->
                chartErosions[Pair(y, x)]!! % 3
            }
        }

//        printChart(10, 10)
        return total
    }

    override fun part2(): Any? {
        findTimes()
        return bestTimes[Pair(target[1], target[0])]!!.first
    }
}