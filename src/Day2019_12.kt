


class Day2019_12(rawInput: List<String>) : Day(rawInput) {


    private val modules: MutableList<Long> = mutableListOf()

    init {
        val parser = """(\d+)""".toRegex()
        for (line in rawInput) {
            val (x) = parser.matchEntire(line)?.destructured ?:
                throw Exception("bad input: $line")
            modules.add(x.toLong())
        }
    }

    override fun part1(): Any? {
        return null
    }


    override fun part2(): Any? {
        return null
    }
}
