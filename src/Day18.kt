import java.util.*

class Day18(rawInput: List<String>) : Day(rawInput) {

    val testInput =  rawInput
//""".#.#...|#.
//.....#|##|
//.|..|...#.
//..|#.....#
//#.#|||#|#|
//...#.||...
//.|....|...
//||...#|.#|
//|.||||..|.
//...#.|..|.""".lines()

    val chart: Array<IntArray> = testInput.map { line ->
        line.map { char ->
            when (char) {
                '|' -> -1 // trees
                '.' -> 0  // open ground
                '#' -> 1  // lumberyard
                else -> throw Exception("bad input: $line")
            }
        }.toIntArray()
    }.toTypedArray()

    val maxX = testInput.first().length - 1
    val maxY = testInput.size - 1

    private fun adjacent(chart: Array<IntArray>, x: Int, y: Int) = sequence {
        if (x > 0)
            yield(chart[y][x - 1])
        if (y > 0)
            yield(chart[y - 1][x])
        if (x < maxX)
            yield(chart[y][x + 1])
        if (y < maxY)
            yield(chart[y + 1][x])
        if (x > 0 && y > 0)
            yield(chart[y - 1][x - 1])
        if (x > 0 && y < maxY)
            yield(chart[y + 1][x - 1])
        if (x < maxX && y < maxY)
            yield(chart[y + 1][x + 1])
        if (x < maxX && y > 0)
            yield(chart[y - 1][x + 1])
    }

    private fun step(input: Array<IntArray>): Array<IntArray> {
        return input.mapIndexed { y, line ->
            line.mapIndexed { x, value ->
                val locale = adjacent(input, x, y)
                when {
                    value == -1 && locale.count { it == 1 } >= 3 ->
                        1
                    value == 1 && (locale.none { it == 1} || locale.none { it == -1 }) ->
                        0
                    value == 0 && locale.count { it == -1 } >= 3 ->
                        -1
                    else ->
                        value
                }
            }.toIntArray()
        }.toTypedArray()
    }

    private fun printChart(chart: Array<IntArray>) {
        chart.forEach { line ->
            println(line.joinToString(separator = "") {
                when (it) {
                    1 -> "#"
                    -1 -> "|"
                    else -> "."
                }
            })
        }
    }

    override fun part1(): Any? {
        var currChart = chart
        for (i in 1..10) {
            currChart = step(currChart)
        }

        val wooded = currChart.sumBy { it.count { it == -1 } }
        val lumberyard = currChart.sumBy { it.count { it == 1 } }
        return "$wooded * $lumberyard = ${wooded * lumberyard}"
    }

    private fun hashImage(chart: Array<IntArray>): Int {
        return Arrays.hashCode(chart.map { Arrays.hashCode(it) }.toTypedArray())
    }

    val allCharts = mutableMapOf<Int, Pair<Int, Array<IntArray>>>()

    override fun part2(): Any? {
        var currChart = chart
        var loopLen = 0
        for (i in 1..1000) {
            currChart = step(currChart)
            if (allCharts.contains(hashImage(currChart))) {
                loopLen = i - allCharts[hashImage(currChart)]!!.first
                break
            }
            allCharts[hashImage(currChart)] = i to currChart
        }

        val loopStart = allCharts[hashImage(currChart)]!!.first
        val remaining = (1000000000 - loopStart) % loopLen

        for (i in 1..remaining)
            currChart = step(currChart)

        val wooded = currChart.sumBy { it.count { it == -1 } }
        val lumberyard = currChart.sumBy { it.count { it == 1 } }
        return "$wooded * $lumberyard = ${wooded * lumberyard}"
    }
}