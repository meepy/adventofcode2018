class Day16(rawInput: List<String>) : Day(rawInput) {

//    Before: [2, 1, 1, 0]
//    5 1 0 1
//    After:  [2, 0, 1, 0]

    val data1 = mutableListOf<Triple<IntArray, IntArray, IntArray>>()
    var data2: List<IntArray>

    init {
        var i = 0
        while (rawInput[i].isNotEmpty()) {
            val (a, b, c) = rawInput.subList(i, i + 3)

            val before = a.removePrefix("Before: [").removeSuffix("]")
                .split(", ").map { it.toInt() }
            val operation = b
                .split(" ").map { it.toInt() }
            val after = c.removePrefix("After:  [").removeSuffix("]")
                .split(", ").map { it.toInt() }

            data1.add(Triple(operation.toIntArray(), before.toIntArray(), after.toIntArray()))
            i += 4
        }

        data2 = rawInput.drop(i + 2).map { line ->
            line.split(" ").map { it.toInt() }
                .toIntArray()
        }
    }

    val operations = listOf<(IntArray, Int, Int, Int) -> Unit>(
        { r, a, b, c -> r[c] = r[a] + r[b] },
        { r, a, b, c -> r[c] = r[a] + b },
        { r, a, b, c -> r[c] = r[a] * r[b] },
        { r, a, b, c -> r[c] = r[a] * b },
        { r, a, b, c -> r[c] = r[a] and r[b] },
        { r, a, b, c -> r[c] = r[a] and b },
        { r, a, b, c -> r[c] = r[a] or r[b] },
        { r, a, b, c -> r[c] = r[a] or b },
        { r, a, b, c -> r[c] = r[a] },
        { r, a, b, c -> r[c] = a },
        { r, a, b, c -> r[c] = if (a > r[b]) 1 else 0 },
        { r, a, b, c -> r[c] = if (r[a] > b) 1 else 0 },
        { r, a, b, c -> r[c] = if (r[a] > r[b]) 1 else 0 },
        { r, a, b, c -> r[c] = if (a == r[b]) 1 else 0 },
        { r, a, b, c -> r[c] = if (r[a] == b) 1 else 0 },
        { r, a, b, c -> r[c] = if (r[a] == r[b]) 1 else 0 }
    )

    private fun findMatches() = data1.map { (byteCode, before, after) ->
        val (op, a, b, c) = byteCode
        op to operations.withIndex().filter { (_, operation) ->
            val input = before.clone()
            operation(input, a, b, c)
            input.contentEquals(after)
        }.map { it.index }
    }

    private fun findOpcodes(): Map<Int, Set<Int>> {
        val opcodeMap = mutableMapOf<Int, MutableSet<Int>>()
        for ((opCode, matches) in findMatches()) {
            if (matches.isEmpty())
                throw Exception("bad input")
            opcodeMap.getOrPut(opCode) { mutableSetOf() }.addAll(matches)
        }

        return opcodeMap
    }

    // remove one value from all map sets and return new map
    private fun removeOne(currMap: Map<Int, Set<Int>>, toRemove: Int): Map<Int, Set<Int>> {
        val newMap = mutableMapOf<Int, MutableSet<Int>>()
        for (entry in currMap) {
            val newSet = entry.value.toMutableSet()
            newSet.remove(toRemove)
            newMap[entry.key] = newSet
        }
        return newMap
    }

    private fun solve(currMap: Map<Int, Set<Int>>): Map<Int, Int>? {
        if (currMap.count { it.value.isEmpty() } > 0)
            return null

        if (currMap.isEmpty()) {
            return mapOf()
        }

        val newMap = currMap.toMutableMap()
        val keyChosen = currMap.keys.first()
        val chooseOne = newMap.remove(keyChosen)!!
        for (value in chooseOne) {
            val result = solve(removeOne(newMap, value))
            if (result != null) {
                val newResult = result.toMutableMap()
                newResult[keyChosen] = value
                return newResult
            }
        }

        return null
    }

    private fun checkSolution(solution: Map<Int, Int>) {
        println(solution.toSortedMap())
        for ((byteCode, before, after) in data1) {
            val (op, a, b, c) = byteCode
            val input = before.clone()
            val operation = operations[solution[op]!!]
            operation(input, a, b, c)
            if (!input.contentEquals(after)) {
                throw Exception("opcode solution failed")
            }
        }
    }

    override fun part1(): Any? {
        val matchedOps = findMatches()
        return "${matchedOps.count { it.second.count() >= 3 }}"
    }

    override fun part2(): Any? {
        val opcodeMap = findOpcodes()
        println(opcodeMap)

        val solution = solve(opcodeMap)!!

        checkSolution(solution)

        val registers = IntArray(4)
        data2.forEach { (op, a, b, c) ->
            operations[solution[op]!!](registers, a, b, c)
        }
        return registers.first()
    }
}