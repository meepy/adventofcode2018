class DancingLinks(rawInput: List<String>) : Day(rawInput) {
    override fun part1(): Any? {
        return "not implemented"
    }

    val data1 = mutableListOf<Triple<IntArray, IntArray, IntArray>>()
    var data2: List<IntArray>

    init {
        var i = 0
        while (rawInput[i].isNotEmpty()) {
            val (a, b, c) = rawInput.subList(i, i + 3)

            val before = a.removePrefix("Before: [").removeSuffix("]")
                .split(", ").map { it.toInt() }
            val operation = b
                .split(" ").map { it.toInt() }
            val after = c.removePrefix("After:  [").removeSuffix("]")
                .split(", ").map { it.toInt() }

            data1.add(Triple(operation.toIntArray(), before.toIntArray(), after.toIntArray()))
            i += 4
        }

        data2 = rawInput.drop(i + 2).map { line ->
            line.split(" ").map { it.toInt() }
                .toIntArray()
        }
    }


    val operations = listOf<(IntArray, Int, Int, Int) -> IntArray>(
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) +   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) *   b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) and b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  get(b)             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a) or  b                  ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, get(a)                        ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, a                             ) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a > get(b))       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > b)       1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) > get(b))  1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (a == get(b))      1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == b)      1 else 0) } },
        { r, a, b, c -> r.copyOf().apply { set(c, if (get(a) == get(b)) 1 else 0) } })

    private fun findMatches() = data1.asSequence().map { (byteCode, before, after) ->
        val (op, a, b, c) = byteCode
        op to operations.withIndex().filter {
            it.value(before, a, b, c).contentEquals(after)
        }.map { it.index }
    }

    private fun findOpcodes(): Map<Int, Set<Int>> {
        val opcodeMap = mutableMapOf<Int, MutableSet<Int>>()
        for ((opCode, matches) in findMatches()) {
            if (matches.isEmpty())
                throw Exception("bad input")
            opcodeMap.getOrPut(opCode) { mutableSetOf() }.addAll(matches)
        }

        return opcodeMap
    }

    private fun checkSolution(solution: Map<Int, Int>) {
        println(solution.toSortedMap())
        for ((byteCode, before, after) in data1) {
            val (op, a, b, c) = byteCode
            val operation = operations[solution[op]!!]
            if (!operation(before, a, b, c).contentEquals(after)) {
                throw Exception("opcode solution failed")
            }
        }
    }

    data class Choice(val opcode: Int, val opIndex: Int) {
        // constraints:
        //     0..15: each operation has one opcode
        //     16..31: each opcode has one operation
        lateinit var constraints: BooleanArray
    }

    private fun buildChoices(opcodeMap: Map<Int, Set<Int>>): List<Choice> {
        val choices = mutableListOf<Choice>()
        for ((opcode, operations) in opcodeMap) {
            for (opIndex in operations) {
                val newChoice = Choice(opcode, opIndex)
                newChoice.constraints = BooleanArray(32).apply {
                    set(opcode, true)
                    set(opIndex + 16, true)
                }
                choices.add(newChoice)
            }
        }
        check(choices.size == opcodeMap.toList().sumBy { it.second.count() })

        return choices
    }

    data class Node(var up: Node?, var down: Node?,
                    var left: Node?, var right: Node?) {

        var columnHeader: Node? = null
        var v1 = 0
        var v2 = 0

        constructor() : this(null, null, null, null) {
            up = this
            down = this
            left = this
            right = this
        }

        constructor(columnHeader: Node?, v1: Int, v2: Int) : this() {
            this.columnHeader = columnHeader
            this.v1 = v1
            this.v2 = v2
        }

        fun insertBefore(toRight: Node) {
            right = toRight
            left = toRight.left
            toRight.left!!.right = this
            toRight.left = this
        }

        fun insertBeforeVertical(toDown: Node) {
            down = toDown
            up = toDown.up
            toDown.up!!.down = this
            toDown.up = this
        }

        fun delete() {
            right!!.left = left
            left!!.right = right
        }

        fun restore() {
            right!!.left = this
            left!!.right = this
        }

        fun deleteVertical() {
            up!!.down = down
            down!!.up = up
        }

        fun restoreVertical() {
            up!!.down = this
            down!!.up = this
        }
    }

    private fun buildMatrix(choices: List<Choice>): Node {
        val numConstraints = choices.first().constraints.size
        val root = Node()

        // build headers (value = column sum)
        val headers = mutableListOf<Node>()
        for (i in 1 .. numConstraints) {
            val header = Node().apply {
                v1 = i
                insertBefore(root)
            }
            headers.add(header)
        }

        // build rows (value = rowNum)
        choices.forEach { choice ->
            var row: Node? = null
            choice.constraints.forEachIndexed { colNum, value ->
                if (!value)
                    return@forEachIndexed

                val column = headers[colNum]
                column.v2 += 1

                Node(column, choice.opcode, choice.opIndex).apply {
                    insertBeforeVertical(column)
                    if (row == null) {
                        row = this
                    } else {
                        insertBefore(row!!)
                    }
                }
            }
        }

        return root
    }

    private fun findMinColumn(root: Node): Node {
        var currNode = root.right!!
        var least = currNode.v2
        var leastNode = currNode

        while (currNode !== root) {
            if (currNode.v2 < least) {
                least = currNode.v2
                leastNode = currNode
            }
            currNode = currNode.right!!
        }

        return leastNode
    }

    private fun cover(column: Node) {
        column.delete()

        var currNode = column.down!!
        while (currNode !== column) {

            var row = currNode.right!!
            while (row !== currNode) {
                row.deleteVertical()
                row.columnHeader!!.v2 -= 1
                row = row.right!!
            }

            currNode = currNode.down!!
        }
    }

    private fun uncover(column: Node) {
        var currNode = column.up!!
        while (currNode !== column) {

            var row = currNode.left!!
            while (row !== currNode) {
                row.restoreVertical()
                row.columnHeader!!.v2 += 1
                row = row.left!!
            }

            currNode = currNode.up!!
        }

        column.restore()
    }

    private fun danceBabyDance(root: Node): MutableList<Pair<Int, Int>>? {
        if (root.right === root)
            return mutableListOf()

        val pick = findMinColumn(root)

        cover(pick)

        var row = pick.down!!
        while (row !== pick) {

            var otherColumns = row.right!!
            while (otherColumns !== row) {
                cover(otherColumns.columnHeader!!)
                otherColumns = otherColumns.right!!
            }

            val solution = danceBabyDance(root)
            if (solution != null) {
                solution.add(row.v1 to row.v2)
                return solution
            }

            otherColumns = row.left!!
            while (otherColumns !== row) {
                uncover(otherColumns.columnHeader!!)
                otherColumns = otherColumns.left!!
            }

            row = row.down!!
        }

        uncover(pick)

        return null
    }

    private fun test() {
        val testData = mapOf(
            0 to setOf(1, 4, 7),
            1 to setOf(1, 4),
            2 to setOf(4, 5, 7),
            3 to setOf(3, 5, 6),
            4 to setOf(2, 3, 6, 7),
            5 to setOf(2, 7))

        val choices = mutableListOf<Choice>()
        for ((opcode, operations) in testData) {
            val newChoice = Choice(opcode, 0)
            newChoice.constraints = BooleanArray(7).apply {
                for (opIndex in operations) {
                    set(opIndex - 1, true)
                }
            }
            choices.add(newChoice)
        }

        val constraints = buildMatrix(choices)
        val solutionList = danceBabyDance(constraints)!!
        check(solutionList.map { it.first }.toSet() == setOf(1, 3, 5))
    }

    override fun part2(): Any? {
        val opcodeMap = findOpcodes()
        println(opcodeMap)

        val choices = buildChoices(opcodeMap)
        val constraints = buildMatrix(choices)
        val solutionList = danceBabyDance(constraints)!!

        val solution = solutionList.toMap()
        checkSolution(solution)

        var registers = IntArray(4)
        data2.forEach { (op, a, b, c) ->
            registers = operations[solution[op]!!](registers, a, b, c)
        }
        return registers.first()
    }
}