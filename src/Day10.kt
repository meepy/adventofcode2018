class Day10(rawInput: List<String>) : Day(rawInput) {
    data class Point(var x: Int, var y: Int) {
        fun add(x: Int, y: Int) {
            this.x += x
            this.y += y
        }
    }

//    val starPositions = mutableListOf<Point>(Point(0, 0), Point(1, 1), Point(2, 2))

    val starPositions = mutableListOf<Point>()
    val starVelocities = mutableListOf<Point>()

    init {
//        position=<-6,  0> velocity=< 2,  0>
        val parser = """position=< *(-?\d+), +(-?\d+)> velocity=< *(-?\d+), +(-?\d+)>""".toRegex()
        for (line in rawInput) {
            val (x, y, vX, vY) = parser.matchEntire(line)?.destructured ?: throw Exception("bad input: ${line.length}, $line")
            starPositions.add(Point(x.toInt(), y.toInt()))
            starVelocities.add(Point(vX.toInt(), vY.toInt()))
        }
    }

    private fun printStars() {
        val minXY = Point(starPositions.minBy { it.x }!!.x, starPositions.minBy { it.y }!!.y)
        val maxXY = Point(starPositions.maxBy { it.x }!!.x, starPositions.maxBy { it.y }!!.y)
        for (y in minXY.y .. maxXY.y) {
            val oneLine = (minXY.x..maxXY.x).map { x ->
                if (starPositions.find { it.x == x && it.y == y } != null)
                    "*"
                else
                    " "
            }
            println("|" + oneLine.joinToString("") + "|")
        }
    }

    private fun oneStep() {
        for (star in starVelocities.withIndex()) {
            val (dX, dY) = star.value
            starPositions[star.index].add(dX, dY)
        }
    }

    override fun part1(): Any? {
        var closely: Boolean
        var i = 0
        do {
            i++
            oneStep()
            val xDiff = starPositions.maxBy { it.x }!!.x - starPositions.minBy { it.x }!!.x
            val yDiff = starPositions.maxBy { it.y }!!.y - starPositions.minBy { it.y }!!.y
            closely = i > 11000 //(xDiff < 200 && yDiff < 200)

//            if (xDiff < 200 && yDiff < 200) { // look at close values
            if (i == 10101) { // found by inspection
                println("$i: $xDiff, $yDiff")
                printStars()
            }
        } while (!closely)

        return null
    }

    override fun part2(): Any? {
        return null
    }
}